import "./App.css";

import { useCallback, useMemo, useState } from "react";

function App() {
  const [count, setCount] = useState(0);
  const [range, setRange] = useState(0);

  const sum = useMemo(() => {
    let calc = 0;
    for (let i = 0; i < range; i++) {
      calc += Number(range);
    }

    console.log("rendering");

    return calc;
  }, [range]);

  const handleClick = useCallback(() => {
    console.log("user clicked button");
  }, []); // function get recreated when dependencies change, here the array is empty meaning the function will never be recreated

  return (
    <>
      <label htmlFor="range">Set limit: </label>
      <input
        id="range"
        name="range"
        value={range}
        onChange={(e) => setRange(e.target.value)}
      />
      <h1>Calculation sum: {sum}</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
      </div>
      <button onClick={handleClick}>Click me!</button>
    </>
  );
}

export default App;
